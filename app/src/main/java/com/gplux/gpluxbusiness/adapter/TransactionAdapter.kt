package com.gplux.gpluxbusiness.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.cardview.widget.CardView
import androidx.core.os.persistableBundleOf
import androidx.recyclerview.widget.RecyclerView
import com.gplux.gpluxbusiness.R
import com.gplux.gpluxbusiness.`interface`.ITransactionClickListener
import com.gplux.gpluxbusiness.model.Transaction

class TransactionAdapter(internal var context: Context, internal var transactionList:List<Transaction>):
    RecyclerView.Adapter<TransactionAdapter.MyViewHolder>() {

    override fun getItemCount(): Int {
        return transactionList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.id.text = transactionList[position].idTransaction
        holder.process.text = transactionList[position].process
        holder.amount.text = String.format("%.2f", transactionList[position].amount!!.toDouble())
        holder.date.text = transactionList[position].date_entity
        holder.entity.text = transactionList[position].entity

        if (position % 2 == 0)
            holder.root_view.setCardBackgroundColor(Color.parseColor("#E1E1E1"))

        holder.setClick(object: ITransactionClickListener{
            override fun onTransactionClick(view: View, position: Int) {
                Toast.makeText(context, transactionList[position].idTransaction, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.transaction_layout, parent, false)
        return MyViewHolder(itemView)
    }


    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var root_view: CardView
        internal var id: TextView
        internal var process: TextView
        internal var amount: TextView
        internal var date: TextView
        internal var entity: TextView

        internal lateinit var transactionClickListener: ITransactionClickListener

        fun setClick(transactionClickListener: ITransactionClickListener)
        {
            this.transactionClickListener = transactionClickListener
        }

        init {
            root_view = itemView.findViewById(R.id.root_ViewTr) as CardView
            id = itemView.findViewById(R.id.trID) as TextView
            process = itemView.findViewById(R.id.trProcess) as TextView
            amount = itemView.findViewById(R.id.trAmount) as TextView
            date = itemView.findViewById(R.id.trDate) as TextView
            entity = itemView.findViewById(R.id.trEntity) as TextView
        }

        override fun onClick(p0: View?) {
            transactionClickListener.onTransactionClick(p0!!, adapterPosition)
        }

    }
}