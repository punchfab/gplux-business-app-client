package com.gplux.gpluxbusiness.model

import java.util.*
import kotlin.math.roundToLong

class Transaction {
    var idTransaction: String? = null
    var process: String? = null
    var amount: String? = null
    var type: String? = null
    val entity: String? = null
    val date_entity: String? = null
    val date: String? = null
}