package com.gplux.gpluxbusiness.model

data class TransactionResponse (val transaction: List<Transaction>)