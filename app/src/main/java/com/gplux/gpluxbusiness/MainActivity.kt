package com.gplux.gpluxbusiness

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.gplux.gpluxbusiness.adapter.TransactionAdapter
import com.gplux.gpluxbusiness.model.Transaction
import com.gplux.gpluxbusiness.model.TransactionResponse
import com.gplux.gpluxbusiness.network.RetrofitClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.Toast
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    //internal lateinit var compositeDiposable = CompositeDiposable()
    internal lateinit var layoutManager: LinearLayoutManager
    internal lateinit var tradapter: TransactionAdapter
    internal lateinit var transactionList: MutableList<Transaction>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvTrans.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        rvTrans.layoutManager = layoutManager
        rvTrans.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))
        transactionList = mutableListOf()
        rvTrans.adapter = TransactionAdapter(applicationContext, transactionList)

        RetrofitClient.instance.getTransaction()
            .enqueue(object: Callback<TransactionResponse> {
                override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<TransactionResponse>,response: Response<TransactionResponse>) {
                    //var array = ArrayList<Transaction>(response.body()?.transaction)
                    //transactionList.addAll()
                    //Toast.makeText(applicationContext, response.body()?.transaction!!.size, Toast.LENGTH_LONG).show()
                    rvTrans.adapter = TransactionAdapter(applicationContext, response.body()?.transaction!!)
                }
            })
    }
}