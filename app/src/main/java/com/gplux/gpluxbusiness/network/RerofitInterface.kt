package com.gplux.gpluxbusiness.network

import com.gplux.gpluxbusiness.model.Transaction
import com.gplux.gpluxbusiness.model.TransactionResponse
import retrofit2.Call
import retrofit2.http.*

interface RetrofitInterface {

    @GET("transactions")
    fun getTransaction(): Call<TransactionResponse>
}