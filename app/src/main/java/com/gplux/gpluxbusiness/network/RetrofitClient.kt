package com.gplux.gpluxbusiness.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    val url = "http://138.197.109.37:3000/"
    //"http://192.168.100.10:8080"
    val instance: RetrofitInterface by lazy{
            val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        //GitHubService service = retrofit.create(GitHubService.class);
        retrofit.create(RetrofitInterface::class.java)
    }
}