package com.gplux.gpluxbusiness.`interface`

import android.view.View

interface ITransactionClickListener {
    fun onTransactionClick(view: View, position: Int)
}